# Machine-Learning-Andrew-Ng

A repository for the course provided by Andrew Ng.

Details of this course can be accessed online.

https://study.163.com/course/introduction/1004570029.htm

Exercises can be accessed at 

https://s3.amazonaws.com/spark-public/ml/exercises/on-demand/machine-learning-ex1.zip

https://s3.amazonaws.com/spark-public/ml/exercises/on-demand/machine-learning-ex2.zip

...

They can also be accessed in **original-exercises** folder in this repository.

1. linear-regression-cpp-implementation

    A simple C++ program implementing gradient descent in linear regression y = a * x + b

    * linear-regression.cpp - main algorithm

    * cost_function_iteration_times.m - draw the cost-iteration plot after running the C++ program