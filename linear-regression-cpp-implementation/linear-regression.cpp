/*
 * A simple C++ program implementing gradient descent in linear regression y = a * x + b
 */
#include <vector>
#include <iostream>
#include <random>
#include <ctime>
#include <cmath>
#include <fstream>

using namespace std;

const double START = -1;
const double END = 1;
const double DISTURBANCE = 0.05; // Disturbance rate, from 0 to 1
const int DATA_SIZE = 100;
const double RATE = 0.0001; // Learning rate, alpha in Andrew Ng's course
const int MAX_ITERATE_TIMES = 500000;
const double TOLERANT = 0.00001;
ofstream file_out("output.data");
vector<double> cost_value;

// generate y = a * x + b with some disturbance
void generate(vector<double> &x, vector<double> &y, double a, double b);

double calculate_a(vector<double> &x, vector<double> &y, double a, double b);

double calculate_b(vector<double> &x, vector<double> &y, double a, double b);

double cost(vector<double> &x, vector<double> &y, double a, double b);

pair<double, double> regression(vector<double> &x, vector<double> &y, double start_a, double start_b);

int main()
{
    vector<double> x;
    vector<double> y;
    double a = 1998;
    double b = 216;
    generate(x, y, a, b);
    double start_a = 0;
    double start_b = 0;
    cost_value.push_back(cost(x, y, start_a, start_b));
    cout << "Original: " << endl;
    cout << a << "\t\t" << b << endl;
    pair<double, double> result = regression(x, y, start_a, start_b);
    cout << "Result: " << endl;
    cout << result.first << "\t\t" << result.second << endl;
    for (double& x : cost_value)
        file_out << x << " ";
    file_out.close();
    return 0;
}

void generate(vector<double> &x, vector<double> &y, double a, double b)
{
    mt19937_64 engine(time(NULL));
    uniform_real_distribution<double> distribution1(START, END);
    uniform_real_distribution<double> distribution2(-DISTURBANCE, DISTURBANCE);
    for (int i = 0; i < DATA_SIZE; ++i)
    {
        // cout << distribution1(engine) << '\t' << distribution2(engine) << endl;
        double this_x = distribution1(engine);
        double this_y = this_x * a + b;
        double disturb = this_y * distribution2(engine);
        x.push_back(this_x);
        y.push_back(this_y + disturb);
    }
}

pair<double, double> regression(vector<double> &x, vector<double> &y, double start_a, double start_b)
{
    for (int i = 0; i < MAX_ITERATE_TIMES; ++i)
    {
        double temp_a = start_a - RATE * calculate_a(x, y, start_a, start_b);
        double temp_b = start_b - RATE * calculate_b(x, y, start_a, start_b);
        double pre_cost = cost_value.back();
        double new_cost = cost(x, y, temp_a, temp_b);
        if (abs(new_cost - pre_cost) < pre_cost * TOLERANT)
            break;
        start_a = temp_a;
        start_b = temp_b;
        cost_value.push_back(new_cost);
        // cout << i << ": " << start_a << "\t\t" << start_b << endl;
    }
    return make_pair(start_a, start_b);
}

double calculate_b(vector<double> &x, vector<double> &y, double a, double b)
{
    double sum = 0;
    for (int i = 0; i < DATA_SIZE; ++i)
    {
        sum += a * x[i] + b - y[i];
    }
    sum = sum / DATA_SIZE;
    return sum;
}

double calculate_a(vector<double> &x, vector<double> &y, double a, double b)
{
    double sum = 0;
    for (int i = 0; i < DATA_SIZE; ++i)
    {
        sum += x[i] * (a * x[i] + b - y[i]);
    }
    sum = sum / DATA_SIZE;
    return sum;
}

double cost(vector<double> &x, vector<double> &y, double a, double b)
{
    double sum = 0;
    for (int i = 0; i < DATA_SIZE; ++i)
    {
        double diff = a * x[i] + b - y[i];
        sum += diff * diff;
    }
    sum /= (2 * DATA_SIZE);
    return sum;
}